﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.DBModel;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для Услуги.xaml
    /// </summary>
    public partial class Услуги : Page
    {
        DbSet<Услуга> услуга;

        public Услуги()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Услуги";
            UpdateList();

            //var employee = PageHelper.DbConnect.Услуга;

            //DGService.ItemsSource = employee.ToList();
        }

        private void UpdateList()
        {
            услуга = PageHelper.DbConnect.Услуга;
            DGService.ItemsSource = услуга.ToList();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Главное_меню());
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
