﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для Поставки.xaml
    /// </summary>
    public partial class Поставки : Page
    {
        public Поставки()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Поставка материалов";

            var employee = PageHelper.DbConnect.Материал;

            DGMaterials.ItemsSource = employee.ToList();
            cmbProduct.ItemsSource = PageHelper.DbConnect.Товары.ToList();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Главное_меню());
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
