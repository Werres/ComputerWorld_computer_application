﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для ДобавитьТовар.xaml
    /// </summary>
    public partial class ДобавитьТовар : Page
    {
        public ДобавитьТовар()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Добавление товара";
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Товары());
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
