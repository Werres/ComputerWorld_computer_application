﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для Главное_меню.xaml
    /// </summary>
    public partial class Главное_меню : Page
    {
        public Главное_меню()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Главное меню";
        }

        private void OrderBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Заказы());
        }

        private void ServiceBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Услуги());
        }

        private void ProductBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Товары());
        }

        private void MaterialsBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Поставки());
        }

        private void ClientBtn_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Клиенты());
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
