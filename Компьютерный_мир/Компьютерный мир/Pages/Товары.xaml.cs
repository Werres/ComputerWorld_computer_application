﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для Товары.xaml
    /// </summary>
    public partial class Товары : Page
    {
        DbSet<Товары> товарыs;
        public Товары()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Товары";
            //UpdateList();

            var employee = PageHelper.DbConnect.Товары;

            DGProduct.ItemsSource = employee.ToList();
        }

        //private void UpdateList()
        //{
            //товарыs = PageHelper.DbConnect.Товары;
            //DGProduct.ItemsSource = товарыs.ToList();
        //}

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Главное_меню());
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.ДобавитьТовар());
        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
