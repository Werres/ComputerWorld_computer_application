﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для Заказы.xaml
    /// </summary>
    public partial class Заказы : Page
    {
        public Заказы()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Заказы";

            var employee = PageHelper.DbConnect.Заказ;

            DGOrder.ItemsSource = employee.ToList();
            cmbFirstName.ItemsSource = PageHelper.DbConnect.Клиент.ToList();
            cmbMiddleName.ItemsSource = PageHelper.DbConnect.Клиент.ToList();
            cmbSecondName.ItemsSource = PageHelper.DbConnect.Клиент.ToList();
            cmbProduct.ItemsSource = PageHelper.DbConnect.Товары.ToList();
            cmbFirstNameEmployee.ItemsSource = PageHelper.DbConnect.Сотрудник.ToList();
            cmbService.ItemsSource = PageHelper.DbConnect.Услуга.ToList();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new Pages.Главное_меню());
        }
    }
}
