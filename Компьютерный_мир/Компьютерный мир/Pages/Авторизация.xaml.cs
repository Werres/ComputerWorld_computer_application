﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Компьютерный_мир.Helpers;

namespace Компьютерный_мир.Pages
{
    /// <summary>
    /// Логика взаимодействия для Авторизация.xaml
    /// </summary>
    public partial class Авторизация : Page
    {
        public Авторизация()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Вход в систему";
        }

        private void SignInBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PageHelper.DbConnect.Сотрудник.Where(x => x.Логин == tLogin.Text && x.Пароль == tPass.Password).FirstOrDefault() == null)
            {
                MessageBox.Show("Неверные данные", "Ошибка входа");
                return;
            }

            if (PageHelper.DbConnect.Сотрудник.Where(x => x.Логин == tLogin.Text && x.Пароль == tPass.Password).FirstOrDefault() != null)
            {
                PageHelper.MainFrame.Navigate(new Pages.Главное_меню());
            }

        }
    }
}
