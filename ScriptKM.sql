USE [master]
GO
/****** Object:  Database [Компьютерный мир]    Script Date: 27.05.2022 12:15:40 ******/
CREATE DATABASE [Компьютерный мир]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Компьютерный мир', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Компьютерный мир.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Компьютерный мир_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Компьютерный мир_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Компьютерный мир] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Компьютерный мир].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Компьютерный мир] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Компьютерный мир] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Компьютерный мир] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Компьютерный мир] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Компьютерный мир] SET ARITHABORT OFF 
GO
ALTER DATABASE [Компьютерный мир] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Компьютерный мир] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Компьютерный мир] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Компьютерный мир] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Компьютерный мир] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Компьютерный мир] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Компьютерный мир] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Компьютерный мир] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Компьютерный мир] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Компьютерный мир] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Компьютерный мир] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Компьютерный мир] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Компьютерный мир] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Компьютерный мир] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Компьютерный мир] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Компьютерный мир] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Компьютерный мир] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Компьютерный мир] SET RECOVERY FULL 
GO
ALTER DATABASE [Компьютерный мир] SET  MULTI_USER 
GO
ALTER DATABASE [Компьютерный мир] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Компьютерный мир] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Компьютерный мир] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Компьютерный мир] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Компьютерный мир] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Компьютерный мир] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Компьютерный мир', N'ON'
GO
ALTER DATABASE [Компьютерный мир] SET QUERY_STORE = OFF
GO
USE [Компьютерный мир]
GO
/****** Object:  Table [dbo].[Заказ]    Script Date: 27.05.2022 12:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Заказ](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDКлиент] [int] NOT NULL,
	[IDТовар] [int] NULL,
	[Количество] [float] NULL,
	[Дата] [date] NOT NULL,
	[IDСотрудник] [int] NOT NULL,
	[IDУслуга] [int] NULL,
 CONSTRAINT [PK_Заказ] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Клиент]    Script Date: 27.05.2022 12:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Клиент](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Фамилия] [nvarchar](max) NOT NULL,
	[Имя] [nvarchar](max) NOT NULL,
	[Отчество] [nvarchar](max) NULL,
	[Телефон] [nvarchar](max) NOT NULL,
	[Адрес проживания] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Клиент] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Материал]    Script Date: 27.05.2022 12:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Материал](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDТовар] [int] NOT NULL,
	[Поставщик] [nvarchar](max) NOT NULL,
	[Дата] [date] NOT NULL,
	[Цена] [decimal](18, 2) NOT NULL,
	[Количество] [float] NOT NULL,
 CONSTRAINT [PK_Материал] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Сотрудник]    Script Date: 27.05.2022 12:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Сотрудник](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Фамилия] [nvarchar](max) NOT NULL,
	[Имя] [nvarchar](max) NOT NULL,
	[Отчество] [nvarchar](max) NULL,
	[Должность] [nvarchar](max) NOT NULL,
	[Заработная_плата] [decimal](12, 2) NOT NULL,
	[Телефон] [nvarchar](50) NOT NULL,
	[Логин] [nvarchar](max) NOT NULL,
	[Пароль] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Сотрудник] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Товары]    Script Date: 27.05.2022 12:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Товары](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nvarchar](max) NOT NULL,
	[Количество] [float] NOT NULL,
	[Цена] [decimal](12, 2) NOT NULL,
 CONSTRAINT [PK_Товары] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Услуга]    Script Date: 27.05.2022 12:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Услуга](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nvarchar](max) NOT NULL,
	[Цена] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Услуга] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Заказ] ON 

INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (1, 1, 80, 1, CAST(N'2022-05-27' AS Date), 12, NULL)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (2, 2, 56, 1, CAST(N'2022-05-29' AS Date), 12, NULL)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (3, 3, NULL, NULL, CAST(N'2022-06-01' AS Date), 5, 3)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (4, 4, NULL, NULL, CAST(N'2022-06-03' AS Date), 7, 1)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (5, 5, NULL, NULL, CAST(N'2022-06-04' AS Date), 7, 2)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (6, 6, 1, 1, CAST(N'2022-06-09' AS Date), 12, NULL)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (7, 7, NULL, NULL, CAST(N'2022-06-09' AS Date), 7, 5)
INSERT [dbo].[Заказ] ([ID], [IDКлиент], [IDТовар], [Количество], [Дата], [IDСотрудник], [IDУслуга]) VALUES (8, 8, 38, 1, CAST(N'2022-06-11' AS Date), 5, 3)
SET IDENTITY_INSERT [dbo].[Заказ] OFF
GO
SET IDENTITY_INSERT [dbo].[Клиент] ON 

INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (1, N'Власова', N'Ева', N'Егоровна', N'+7 876 399-09-55', N'ул. Гагарина, 7')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (2, N'Латышева', N'Ариана', N'Адамовна', N'+7 567 878-22-33', N'пр. Вишневый, 3')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (3, N'Царев', N'Лев', N'Андреевич', N'+7 809 555-02-88', N'пр. Юбилейный, 2')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (4, N'Павлов', N'Степан', N'Маркович', N'+7 905 444-93-02', N'ул. Володарского, 11')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (5, N'Савицкий', N'Юрий', N'Константинович', N'+7 490 340-88-27', N'ул. Северная, 4')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (6, N'Борисова', N'Алиса', N'Эмировна', N'+7 962 467-67-34', N'ул. Егорьевская, 3')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (7, N'Маркелова', N'Мария', N'Степанова', N'+7 480 590-45-61', N'ул. Гагарина, 4')
INSERT [dbo].[Клиент] ([ID], [Фамилия], [Имя], [Отчество], [Телефон], [Адрес проживания]) VALUES (8, N'Черных', N'Софья', N'Мироновна', N'+7 405 560-88-14', N'ул. Красноармейская, 5')
SET IDENTITY_INSERT [dbo].[Клиент] OFF
GO
SET IDENTITY_INSERT [dbo].[Материал] ON 

INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (1, 3, N'AEROCOOL', CAST(N'2022-05-22' AS Date), CAST(1900.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (2, 4, N'EXEGATE', CAST(N'2022-05-25' AS Date), CAST(2750.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (3, 5, N'AEROCOOL', CAST(N'2022-05-22' AS Date), CAST(3550.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (4, 6, N'ACCORD', CAST(N'2022-05-19' AS Date), CAST(1475.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (5, 7, N'CHIEFTEC', CAST(N'2022-05-19' AS Date), CAST(3250.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (6, 8, N'ACCORD', CAST(N'2022-05-19' AS Date), CAST(2300.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (7, 9, N'INWIN', CAST(N'2022-05-15' AS Date), CAST(1990.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (8, 10, N'AEROCOOL', CAST(N'2022-05-22' AS Date), CAST(4200.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (9, 23, N'SVEN', CAST(N'2022-05-07' AS Date), CAST(1650.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (10, 27, N'DEVENDER', CAST(N'2022-05-03' AS Date), CAST(950.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (11, 52, N'AMD', CAST(N'2022-05-04' AS Date), CAST(3790.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (12, 53, N'INTEL', CAST(N'2022-05-04' AS Date), CAST(6900.00 AS Decimal(18, 2)), 5)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (13, 55, N'INTEL', CAST(N'2022-05-04' AS Date), CAST(6300.00 AS Decimal(18, 2)), 4)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (14, 57, N'PHILIPS', CAST(N'2022-05-14' AS Date), CAST(4700.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (15, 60, N'DEFENDER', CAST(N'2022-05-03' AS Date), CAST(450.00 AS Decimal(18, 2)), 5)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (16, 61, N'GENIUS', CAST(N'2022-05-03' AS Date), CAST(500.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (17, 64, N'DEFENDER', CAST(N'2022-05-03' AS Date), CAST(215.00 AS Decimal(18, 2)), 5)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (18, 65, N'SENNHEISER', CAST(N'2022-05-23' AS Date), CAST(5325.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (19, 66, N'SVEN', CAST(N'2022-05-07' AS Date), CAST(275.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (20, 68, N'RITMIX', CAST(N'2022-05-20' AS Date), CAST(755.00 AS Decimal(18, 2)), 4)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (21, 71, N'SVEN', CAST(N'2022-05-07' AS Date), CAST(465.00 AS Decimal(18, 2)), 5)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (22, 78, N'AMD', CAST(N'2022-05-04' AS Date), CAST(1250.00 AS Decimal(18, 2)), 6)
INSERT [dbo].[Материал] ([ID], [IDТовар], [Поставщик], [Дата], [Цена], [Количество]) VALUES (23, 79, N'INTEL', CAST(N'2022-05-04' AS Date), CAST(4780.00 AS Decimal(18, 2)), 5)
SET IDENTITY_INSERT [dbo].[Материал] OFF
GO
SET IDENTITY_INSERT [dbo].[Сотрудник] ON 

INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (1, N'Волостнов', N'Михаил', N'Николаевич', N'Директор', CAST(60000.00 AS Decimal(12, 2)), N'+7 123 456-78-90', N'klh7pi4rcbtz@gmail.com', N'2L6KZG')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (2, N'Михайлов', N'Андрей', N'Денисович', N'Старший менеджер', CAST(45000.00 AS Decimal(12, 2)), N'+7 239 309-00-99', N'gn0354mbiork@outlook.com', N'uzWC67')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (3, N'Басова', N'Амина', N'Кирилловна', N'Менеджер', CAST(39500.00 AS Decimal(12, 2)), N'+7 345 888-45-55', N'1o4l05k8dwpv@yahoo.com', N'8ntwUp')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (4, N'Сидоров', N'Егор', N'Александрович', N'Старший программист', CAST(46575.00 AS Decimal(12, 2)), N'+7 962 622-13-41', N'hsqixl2vebuz@mail.com', N'YOyhfR')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (5, N'Аксенова', N'Ульяна', N'Ивановна', N'Программист', CAST(41355.00 AS Decimal(12, 2)), N'+7 555 123-45-10', N'towkse0hf26b@outlook.com', N'RSbvHv')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (6, N'Васильева', N'Камила', N'Ивановна', N'Jinior-программист', CAST(39900.00 AS Decimal(12, 2)), N'+7 905 179-10-76', N'khx0ncdwz4uj@gmail.com', N'rwVDh9')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (7, N'Ильин', N'Артём', N'Родионович', N'Системный администратор', CAST(35000.00 AS Decimal(12, 2)), N'+7 098 765-43-21', N'01zji3wfuq7h@outlook.com', N'LdNyos')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (8, N'Васильева', N'Василиса', N'Фёдоровна', N'Помощник администратора', CAST(30990.00 AS Decimal(12, 2)), N'+7 367 982-38-61', N'am65k18q7bwp@mail.com', N'gynQMT')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (9, N'Кудрявцева', N'Василиса', N'Матвеевна', N'Специалист по подбору кадров', CAST(29785.00 AS Decimal(12, 2)), N'+7 509 555-17-56', N'wt9q8i6ypx47@outlook.com', N'AtnDjr')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (10, N'Малыгин', N'Данил', N'Андреевич', N'Менеджер по рекламе', CAST(29500.00 AS Decimal(12, 2)), N'+7 478 451-67-09', N'4o72gufv3xlz@tutanota.com', N'JlFRCZ')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (11, N'Давыдов', N'Данил', N'Алексасндрович', N'Специалист по продвижению проектов', CAST(29500.00 AS Decimal(12, 2)), N'+7 356 666-25-66', N'towkse0hk18q7bwp@mail.com', N'QWERTY')
INSERT [dbo].[Сотрудник] ([ID], [Фамилия], [Имя], [Отчество], [Должность], [Заработная_плата], [Телефон], [Логин], [Пароль]) VALUES (12, N'Алимова', N'Ирина', N'Олеговна', N'Кассир', CAST(25500.00 AS Decimal(12, 2)), N'+7 409 490-44-05', N'оро4o72guf4uj@gmail.com', N'12345')
SET IDENTITY_INSERT [dbo].[Сотрудник] OFF
GO
SET IDENTITY_INSERT [dbo].[Товары] ON 

INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (1, N'Видеокарта ASUS RADEON R7 250 2GB DDR5', 2, CAST(8540.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (2, N'Видеокарта PCI-E 2048MB GT1030 PALIT 64BIT DDR5 OEM', 3, CAST(7990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (3, N'Блок питания ATX 500W AEROCOOL', 1, CAST(2480.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (4, N'Блок питания ATX 600W EXEGATE', 1, CAST(3120.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (5, N'Блок питания ATX 700W AEROCOOL VX SERIES', 1, CAST(4120.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (6, N'Блок питания ATX 450W ACCORD', 1, CAST(1990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (7, N'Блок питания ATX 500W CHIEFTEC', 1, CAST(3890.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (8, N'Блок питания ATX 500W ACCORD', 1, CAST(2790.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (9, N'Блок питания ATX 500W INWIN', 1, CAST(2380.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (10, N'Блок питания ATX 800W AEROCOLL', 1, CAST(4790.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (11, N'Накопитель SDD QUMO 60GB', 5, CAST(1890.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (12, N'Накопитель SSD KINGSTON 240GB A400', 8, CAST(3060.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (13, N'Накопитель SSD KINGSTON 120GB A400', 4, CAST(2680.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (14, N'Жесткий диск 500GB 2.5″ 128MB SEAGATE MOBILE', 10, CAST(3640.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (15, N'Жесткий диск 1000GB 2.5″ 8MB WD SCORPIO BLUE', 7, CAST(5440.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (16, N'Жесткий диск 4000GB 3.5″ ДЛЯ ВИДЕОНАБЛЮДЕНИЯ', 4, CAST(18190.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (17, N'Жесткий диск 500GB 3.5″ TOSHIBA 64MB', 9, CAST(3780.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (18, N'Звуковая карта SB C-MEDIA 8738 4CHANNEL', 12, CAST(780.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (19, N'Звуковая карта TRAA71 (C-MEDIA CM108) USB 2.0', 7, CAST(640.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (20, N'Звуковая карта TRUA3D (C-MEDIA CM108) USB 2.0', 18, CAST(570.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (21, N'Аккумулятор для ИБП SVEN SV1290 (9А/Ч, 12В)', 4, CAST(1790.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (22, N'Аккумулятор для ИБП SVEN SV1272 (7.2А/Ч, 12В)', 5, CAST(1480.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (23, N'Аккумулятор для ИБП SVEN SV12120 (12А/Ч, 12В)', 1, CAST(2090.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (24, N'Аккумулятор для ИБП SVEN SV1213S (1.3А/Ч, 12В)', 3, CAST(640.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (25, N'Клавиатура A4 Q135 BLOODY NEON USB', 2, CAST(1680.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (26, N'Клавиатура A4 KR-85 ПРОВОДНАЯ USB', 4, CAST(890.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (27, N'Клавиатура DEFENDER CHIMERA GK-280DL', 1, CAST(1520.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (28, N'Клавиатура DEFENDER ELEMENT HB-520 , USB(ЧЕРНЫЙ)', 6, CAST(390.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (29, N'Клавиатура DIALOG KS-030U ПРОВОДНАЯ, ЧЕРНАЯ, USB', 4, CAST(390.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (30, N'Клавиатура GENIUS SLIM STAR 335 USB, ИГРОВАЯ', 3, CAST(2380.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (31, N'Клавиатура LOGITECH K120 CLASSIC USB', 5, CAST(790.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (32, N'Клавиатура OKLICK 505 WHITE, SLIM', 4, CAST(620.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (33, N'Колонки DEFENDER MERCURY 35(MK II) 2.0 (2*18ВТ)', 2, CAST(4820.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (34, N'Колонки DIALOG COLIBRI AC-02UP B-W 2.0 2*2,5W USB', 6, CAST(390.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (35, N'Колонки DIALOG AST-25UP BK 2.0 6W USB', 4, CAST(1440.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (36, N'Колонки GEMBIRD SPK-201 2.0, USB', 4, CAST(690.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (37, N'Колонки GEMBIRD SPK-100W 2.0, USB', 8, CAST(680.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (38, N'Компьютер С494142Ц-INTEL PENTIUM G4400 / H110M PRO-VD / 4GB', 1, CAST(24990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (39, N'Компьютер С510108Ц-INTEL CORE I5 7400 / H110M PRO VD/ 8GB', 1, CAST(36990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (40, N'Компьютер ACER VERITON EX2620G [J4005/4GB/128GB SSD/LINUX]', 1, CAST(18980.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (41, N'Компьютер C632743Ц NL-INTEL CORE I3-10100/8GB/SSD240GB/DOS', 1, CAST(35980.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (42, N'Компьютер C649589Ц NL-INTEL CORE I5-10400/8GB/SSD240GB/DOS', 1, CAST(39840.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (43, N'Корпус SUPERPOWER MIDITOWER SP WINARD 3010 C 450W USB BLACK-SILVER', 1, CAST(3290.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (44, N'Корпус SUPERPOWER MIDITOWER SP WINARD 3067 БЕЗ БП', 1, CAST(2240.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (45, N'Кулер AMD IGLOO A360CU LIGHT(E) AM2/AM2+/AM3/FM1', 4, CAST(790.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (46, N'Кулер INTEL ICEHAMMER IH-3075WV FOR SOCKET 775/AM2', 5, CAST(590.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (47, N'Кулер COOLER INTEL ORIGINAL S1156 (AL)', 7, CAST(420.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (48, N'Кулер для видео карты VC-RE GEMBIRD', 5, CAST(760.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (49, N'Кулер для корпусов ZALMAN ZM-F4', 4, CAST(1240.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (50, N'Кулер для корпуса 92X92X25MM SLEEVE', 9, CAST(220.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (51, N'Материнская плата AMD ASROCK 760GM-HDV AM3', 3, CAST(4940.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (52, N'Материнская плата AMD M/B MSI A68HM-E33 V2 USB3.0 RTL FM2+', 0, CAST(4540.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (53, N'Материнская плата INTEL M/B ASUS PRIME B360M-D RTL', 0, CAST(8790.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (54, N'Материнская плата INTEL M/B ASUS PRIME H410R-SI WHITE BOX (S1200)', 2, CAST(7490.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (55, N'Материнская плата INTEL M/B ASUS PRIME H410M-K', 0, CAST(7780.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (56, N'Монитор Ж/К 21.5″ LG 22MK400A-B BLACK', 1, CAST(7460.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (57, N'Монитор Ж/К 21,5″ PHILIPS 223V5LSB2/10(62) BLACK', 0, CAST(6120.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (58, N'Монитор Ж/К 19.5″ PHILIPS 203V5LSB26 (10/62)', 2, CAST(7150.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (59, N'Мышь DEFENDER OPTIMUM (MM-) 140 USB S', 4, CAST(250.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (60, N'Мышь DEFENDER GM-090L SKY DRAGON ПРОВОДНАЯ USB ИГРОВАЯ', 0, CAST(540.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (61, N'Мышь GENIUS MINI NAVIGATOR USB SILVER БЕСПРОВОДНАЯ ОПТ.', 1, CAST(690.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (62, N'Мышь GENIUS TRAVELER 525TC LASER USB ЛАЗЕР. ПРОВОДНАЯ', 3, CAST(690.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (63, N'Мышь OKLICK 745G', 1, CAST(980.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (64, N'Микрофон DEFENDER MIC-130', 0, CAST(270.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (65, N'Микрофон МИКРОФОН SENNHEISER E 835', 1, CAST(6990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (66, N'Микрофон SVEN MK-200', 1, CAST(320.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (67, N'Наушники DEFENDER HN-930 БЕСПРОВОДНЫЕ', 2, CAST(690.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (68, N'Наушники RITMIX RH-533USB', 0, CAST(890.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (69, N'Наушники RITMIX RH-140 SPACER GREY', 3, CAST(280.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (70, N'Наушники SVEN AP-940MV С МИКРОФОНОМ', 2, CAST(1540.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (71, N'Наушники SVEN AP-700 С МИКРОФОНОМ', 0, CAST(620.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (72, N'Наушники SVEN E-135', 2, CAST(270.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (73, N'Ноутбук HP 250 G7 15.6″ HD I3-7020U/8GB/256GB SSD/DOS', 1, CAST(36990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (74, N'МФУ PANASONIC KX-MB2110RUW', 2, CAST(13950.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (75, N'ПО WINDOWS VISTA HOME BASIC (OEI)', 10, CAST(3990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (76, N'Антивирус KASPERSKY INTERNET SECURITY 3-DEVICE 1 YEAR', 5, CAST(1820.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (77, N'Антивирус АНТИВИРУС NOD32', 3, CAST(1040.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (78, N'Процессор CPU AMD A4 6300K (OEM) 3.7ГГЦ FM2', 0, CAST(1990.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (79, N'Процессор CPU INTEL CORE I3-10100F', 0, CAST(11640.00 AS Decimal(12, 2)))
INSERT [dbo].[Товары] ([ID], [Название], [Количество], [Цена]) VALUES (80, N'Процессор CPU INTEL PENTIUM G4400 3.3 ГГЦ (1151) OEM', 3, CAST(7290.00 AS Decimal(12, 2)))
SET IDENTITY_INSERT [dbo].[Товары] OFF
GO
SET IDENTITY_INSERT [dbo].[Услуга] ON 

INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (1, N'Ремонт системного блока', N'от 100 руб. до 1500 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (2, N'Ремонт ноутбука', N'от 300 руб. до 2000 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (3, N'Работа с программным обеспечением', N'от 250 руб. до 1500 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (4, N'Ремонт оргтехники', N'от 400 руб. до 1200 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (5, N'Настройка сетевого оборудования', N'от 50 руб. до 2500 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (6, N'Ремонт и заправка картриджей', N'от 200 руб. до 500 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (7, N'Установка оборудования', N'от 100 руб. до 2500 руб.')
INSERT [dbo].[Услуга] ([ID], [Название], [Цена]) VALUES (8, N'Дополнительные услуги сервисного центра', N'от 10 руб. до 1000 руб.')
SET IDENTITY_INSERT [dbo].[Услуга] OFF
GO
ALTER TABLE [dbo].[Заказ]  WITH CHECK ADD  CONSTRAINT [FK_Заказ_Клиент] FOREIGN KEY([IDКлиент])
REFERENCES [dbo].[Клиент] ([ID])
GO
ALTER TABLE [dbo].[Заказ] CHECK CONSTRAINT [FK_Заказ_Клиент]
GO
ALTER TABLE [dbo].[Заказ]  WITH CHECK ADD  CONSTRAINT [FK_Заказ_Сотрудник] FOREIGN KEY([IDСотрудник])
REFERENCES [dbo].[Сотрудник] ([ID])
GO
ALTER TABLE [dbo].[Заказ] CHECK CONSTRAINT [FK_Заказ_Сотрудник]
GO
ALTER TABLE [dbo].[Заказ]  WITH CHECK ADD  CONSTRAINT [FK_Заказ_Товары] FOREIGN KEY([IDТовар])
REFERENCES [dbo].[Товары] ([ID])
GO
ALTER TABLE [dbo].[Заказ] CHECK CONSTRAINT [FK_Заказ_Товары]
GO
ALTER TABLE [dbo].[Заказ]  WITH CHECK ADD  CONSTRAINT [FK_Заказ_Услуга] FOREIGN KEY([IDУслуга])
REFERENCES [dbo].[Услуга] ([ID])
GO
ALTER TABLE [dbo].[Заказ] CHECK CONSTRAINT [FK_Заказ_Услуга]
GO
ALTER TABLE [dbo].[Материал]  WITH CHECK ADD  CONSTRAINT [FK_Материал_Товары] FOREIGN KEY([IDТовар])
REFERENCES [dbo].[Товары] ([ID])
GO
ALTER TABLE [dbo].[Материал] CHECK CONSTRAINT [FK_Материал_Товары]
GO
USE [master]
GO
ALTER DATABASE [Компьютерный мир] SET  READ_WRITE 
GO
